local M = {}

function M.ts_setup(on_attach, capabilities)
  return {
    on_attach = on_attach,
    capabilities = capabilities,
    flags = {
      debounce_text_changes = 150,
    }
  }
end

function M.eslint_setup(on_attach, capabilities)
  return {
    on_attach = on_attach,
    capabilities = capabilities,
  }
end

return M
